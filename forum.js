// ==UserScript==
// @name         Comment finder
// @namespace    http://your.homepage/
// @version      0.1
// @description  enter something useful
// @author       You
// @include      http://www.glibertarians.com/*
// @include      http*://reason.com/*
// @include      about:blank
// @grant        none
// ==/UserScript==

var commentPage = false;
var commentSystem = "";
var commentEle;
var dragee;
var debug = 1;

if ( document.getElementsByClassName('fb-comments').length > 0 ) {
    commentPage = true;
    commentSystem = "facebook";
}
else if ( document.getElementById('echo_container_placeholder') ) {
    commentPage = true;
    commentSystem = "washPost";
}
else if ( document.getElementById('disqus_thread') || document.body.innerHTML.match('disqus.com') ) {
    commentPage = true;
    commentSystem = "disqus";
}
else if ( document.getElementById('wp-toolbar') && document.getElementsByClassName('commentlist').length > 0 ) {
    commentPage = true;
    commentSystem = "wordpress";
    commentEle = document.getElementsByClassName('commentlist')[0];
}
else if ( document.getElementById('comments') || document.getElementsByClassName('comments-box').length > 0) {
    commentPage = true;
    commentSystem = "generic";
}

function retStylesheet(id) {
    for ( var i=0; i < document.styleSheets.length; i++ ) {
        if ( document.styleSheets[i].ownerNode.id === id ) {
            return document.styleSheets[i].ownerNode;
        }
    }
    return false;
}

function setStylesheet() {
    var domain = document.URL.split("/")[2];
    if ( domain === "reason.com" ) {
        var ssName = "greason-css";
        if ( ! ( ss = retStylesheet(ssName) ) ) {
            ss = document.createElement('style');
            ss.id = ssName;
            ss.type = "text/css";
            document.head.appendChild(ss);
        }
        //don't display videos when picked up in user links
        ss.append("p.meta span.vidSpanClass { display: none; } ");
        //needed to fix a problem with the draggable divs.. couldn't drag them when "below the fold"
        ss.append("body { height: auto; } ");
    }
    /*
    if ( commentSystem === "wordpress" ) {
        var ssName = "wp-css";
        if ( ! ( ss = retStylesheet(ssName) ) ) {
            ss = document.createElement('style');
            ss.id = ssName;
            ss.type = "text/css";
            document.head.appendChild(ss);
        }
        ss.append("span.fn span.vidSpanClass { display: none; } ");
    }
    */
}

function formatImages(ele) {
    var imgs = ele.getElementsByTagName('img');
    var altTextSpanClass = 'imgAlt';
    for(var i=0; i<imgs.length; i++) {
        if (imgs[i].parentNode.getAttribute('class') != altTextSpanClass && (imgs[i].alt.length > 0 || imgs[i].title.length > 0)) {
            var sp = document.createElement('span');
            sp.className = altTextSpanClass;
            imgs[i].parentNode.insertBefore(sp,imgs[i]);
            sp.appendChild(imgs[i]);
            if ( imgs[i].alt.length > 0 ) {
                sp.setAttribute("alt-text",imgs[i].alt);
            }
            else if ( imgs[i].title.length > 0 ) {
                sp.setAttribute("alt-text",imgs[i].title);
            }
        }
    }
}

function formatLinks(ele) {
    var links = ele.getElementsByTagName('a');
    var imgSpanClass = 'imgSpanClass';
    var vidSpanClass = 'vidSpanClass';
    for (var i=0; i < links.length; i++ ) {
        if (links[i].href.match(/.(jpg|jpeg|png|gif)$/) && links[i].parentNode.getAttribute('class') != imgSpanClass ) {
            var src = links[i].href;
            if ( document.URL.split(":")[0] == 'https' ) {
                src = links[i].href.replace(/http:\/\//,"https://");
            }
            links[i].onmouseover = function(evt) { console.log("hovered over ", evt, evt.toElement.href, evt.toElement.href.height); var im=evt.toElement.parentNode.getElementsByTagName('img')[0]; im.style.display=''; im.style.top=(evt.layerY - 40 ) + "px"; im.style.left=( evt.layerX - 40) + "px";};
            var sp = document.createElement('span');
            sp.className = imgSpanClass;
            links[i].parentNode.insertBefore(sp,links[i]);
            var im = document.createElement('img');
            im.onmouseout = function(evt) { console.log("hovered off ", evt, evt.toElement); var im=evt.target; im.style.display='none';};
            im.style.display = "none";
            im.style.position = "absolute";
            im.style.float = "right";
            im.style['z-index'] = 200;
            im.overflow = "auto";
         //   im.style.padding = "20px";
            im.src=src;
            sp.appendChild(im);
            sp.appendChild(links[i]);
        }
        else if ( ( links[i].href.match(/(liveleak|vimeo|youtube).com\//) || links[i].href.match(/youtu.be/) ) && links[i].parentNode.getAttribute('class') != vidSpanClass ) {
            var src = links[i].href;
            if ( debug ) { console.log(src); }
            var proto = src.match(/^https?/i);
            if ( document.URL.split(":")[0] == 'https' ) {
                src = links[i].href.replace(/http:\/\//,"https://");
                proto = "https";
            }
            links[i].onmouseover = function(evt) { console.log("hovered over ", evt, evt.toElement.href); var im=evt.toElement.parentNode.getElementsByTagName('iframe')[0]; im.style.display=''; im.style.top=(evt.layerY - 40 ) + "px"; im.style.left=( evt.layerX - 40) + "px";};
            var sp = document.createElement('span');
            sp.className = vidSpanClass;
            links[i].parentNode.insertBefore(sp,links[i]);
            var newFrame = document.createElement('iframe');
            newFrame.onmouseout = function(evt) { console.log("hovered off ", evt, evt.toElement); var im=evt.target; im.style.display='none';};
            newFrame.style.display = "none";
            newFrame.style.position = "absolute";
            newFrame.style.float = "right";
            newFrame.style['z-index'] = 200;
            newFrame.width = 640;
            newFrame.height = 360;
            newFrame.frameborder = 0;
            newFrame.allowFullscreen=true;
            if ( links[i].href.match(/liveleak/) ) {
                newFrame.className="liveleak-player";
                newFrame.title="Liveleak video player";
                src = src.replace(/(player.|)vimeo.com\/(m\/)?/,"player.vimeo.com/video/");
            }
            else if ( links[i].href.match(/vimeo/) ) {
                newFrame.className="vimeo-player";
                newFrame.title="Vimeo video player";
                src = src.replace("/view","/ll_embed");
            }
            else {
                newFrame.className="youtube-player";
                newFrame.title="Youtube video player";
                var idmatches = src.match(/https?:\/\/[w]{0,3}m?\.?(?:youtu\.be\/([a-z0-9_]+)|youtube\.com\/.*(?:watch|v)=([a-z0-9_]+))/i);
                var id = ( idmatches[1] ) ? idmatches[1] : idmatches[2];
                var startmatch = src.match(/(?:#t|&t|\?t|&start)=([0-9smh]+)/i);
                start = ( startmatch) ? "&t=" + startmatch[1] : "";
                src = proto + "://www.youtube.com/embed/" + id + start;
            }
            newFrame.src = src;
            sp.appendChild(newFrame);
            links[i].parentNode.insertBefore(sp,links[i].nextSibling);
        }
    }
}

function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

function splitTheColorDifference(fromColor,toColor,pct=50) {
    f1=parseInt(fromColor.substring(0,2),16);
    f2=parseInt(fromColor.substring(2,4),16);
    f3=parseInt(fromColor.substring(4,6),16);
    t1=parseInt(toColor.substring(0,2),16);
    t2=parseInt(toColor.substring(2,4),16);
    t3=parseInt(toColor.substring(4,6),16);
    n1=Math.round((t1-f1)*(pct/100) + f1);
    n2=Math.round((t2-f2)*(pct/100) + f2);
    n3=Math.round((t3-f3)*(pct/100) + f3);
    return (n1 * 256 * 256 + n2 * 256 + n3).toString(16);
}


function drag_start(event) {
    var style = window.getComputedStyle(event.target, null);
    dragee = event.target;
    if ( debug ) { console.log("START DRAG: dragee=",dragee, " left=", parseInt(style.getPropertyValue("left"),10), " bottom=", parseInt(style.getPropertyValue("bottom"),10), " clientX=", event.clientX, " clientY=", event.clientY);}
    event.dataTransfer.setData("text/plain", parseInt(style.getPropertyValue("left"),10) + ',' + parseInt(style.getPropertyValue("bottom"),10) + "," + event.clientX + ',' + event.clientY);
} 
function drag_over(event) { 
    event.preventDefault(); 
    return false; 
} 
function drop(event) { 
    var offset = event.dataTransfer.getData("text/plain").split(',');
    dragee.style.left = (parseInt(offset[0],10) + ( event.clientX -parseInt(offset[2],10) )) +  'px';
    dragee.style.bottom = (parseInt(offset[1],10) + ( parseInt(offset[3],10) - event.clientY )) + 'px';
    var form = document.getElementById(dragee.id + "Form");
    if ( form.style.display == "block") {
        localStorage[dragee.id + "FloaterAtRest"] = (dragee.style.left.replace(/px/,"") + "," + dragee.style.bottom.replace(/px/,""));
    }
    else {
        localStorage[dragee.id + "Floater"] = (dragee.style.left.replace(/px/,"") + "," + dragee.style.bottom.replace(/px/,""));
    }

    dragee = '';
    event.preventDefault();
    return false;
} 

function asideClick(event) {
    //display or hide the floater element when clicked
    testtarget=event.target;
    if ( testtarget.id !== "") {
        if ( testtarget.tagName == "ASIDE" ) {
            truetarget=testtarget;
        }
        else {
            truetarget=testtarget.parentNode;
        }
        var form=document.getElementById(truetarget.id + "Form");
        var xy;
        if ( form.style.display == "none") {
            xy = localStorage[truetarget.id + "FloaterAtRest" ];

            form.style.display="block";
            testtarget.innerHTML=testtarget.innerHTML.replace(RegExp("[^<]*" + truetarget.id), truetarget.id);

        }
        else {
            xy = localStorage[truetarget.id + "Floater" ];
            form.style.display="none";
            testtarget.innerHTML=testtarget.innerHTML.replace(truetarget.id, "Show " + truetarget.id);

        }
        truetarget.style.left=xy.split(",")[0] + "px";
        truetarget.style.bottom=xy.split(",")[1] + "px";
    }

}


function makeFloater(name,xy="0,0") {
    if ( debug ) { console.log("making floater", xy); }
    aside=document.createElement('aside');
    aside.id=name;
    aside.draggable=true;
    asideHead=document.createElement('span');
    asideHead.innerHTML="&nbsp;Show " + name + "&nbsp;";
    asideHead.id=name + "Header";
    aside.appendChild(asideHead);
    asideForm=document.createElement('div');
    asideForm.id=name + "Form";
    aside.classList.add('config');
    asideForm.style.display="none";

    aside.appendChild(asideForm);

    if ( typeof(localStorage[name + "Floater"]) == 'undefined' ) {
        localStorage[name + "Floater"]=xy;
        localStorage[name + "FloaterAtRest"]=xy;
    }
    aside.style.left=xy.split(",")[0] + "px";
    aside.style.bottom=xy.split(",")[1] + "px";
    console.log(aside);
    document.body.appendChild(aside);
    aside.addEventListener('dragstart',drag_start,false);
    asideHead.addEventListener('click',asideClick);
    return aside;
}

function openconftab(event) {
    var i;
    configTab=event.target.innerHTML;
    for ( var xx in event.target.parentNode.parentNode.children ) {
        var xxx=event.target.parentNode.parentNode.children[xx];
        if ( typeof(xxx.classList) != 'undefined' && xxx.classList.length > 0 ) {
            xxx.classList.remove("selected");
        }
    }
    event.target.parentNode.classList.add("selected");
    
    var x = document.getElementsByClassName("ConfigTab");
    for (i = 0; i < x.length; i++) {
        if ( debug ) { console.log("hiding ",x[i]); }
        x[i].style.display = "none"; 
    }
    document.getElementById("Config" + configTab).style.display = "block"; 
    
}

function setupConfig() {
    document.body.addEventListener('dragover',drag_over,false);
    document.body.addEventListener('drop',drop,false);


    confStyleEle = document.createElement('style');
    confStyleEle.type = 'text/css';
    confStyleEle.id = 'confStylin';
    confStyleEle.textContent = '.config { border-radius: 8px; position: fixed; background #FFFFFF; border: 1px solid rgba(0,0,0,0.5); border-radius: 3px; padding: 3px; font-size: 12px; font-family: verdana;box-shadow: 2px 2px 5px #888888;';
    document.head.appendChild(confStyleEle);
    tabStyleEle = document.createElement('style');
    tabStyleEle.type = 'text/css';
    tabStyleEle.id = 'tabStylin';
    tabStyleEle.textContent = ' .tabrow { text-align: center; list-style: none; padding: 0; line-height: 24px; height: 20px; overflow: hidden; font-size: 12px; font-family: verdana; position: relative; } ' +
        '.tabrow li { border: 1px solid #AAA; background: #D1D1D1; background: -o-linear-gradient(top, #ECECEC 50%, #D1D1D1 100%); background: -ms-linear-gradient(top, #ECECEC 50%, #D1D1D1 100%); background: -moz-linear-gradient(top, #ECECEC 50%, #D1D1D1 100%); background: -webkit-linear-gradient(top, #ECECEC 50%, #D1D1D1 100%); background: linear-gradient(top, #ECECEC 50%, #D1D1D1 100%); display: inline-block; position: relative; z-index: 0; border-top-left-radius: 6px; border-top-right-radius: 6px; box-shadow: 0 3px 3px rgba(0, 0, 0, 0.4), inset 0 1px 0 #FFF; text-shadow: 0 1px #FFF; margin: 0 -5px; padding: 0 20px; } ' +
        '.tabrow a { color: #555; text-decoration: none; } ' +
        '.tabrow li.selected { background: #FFF; color: #333; z-index: 2; border-bottom-color: #FFF; } ' +
        '.tabrow:before { position: absolute; content: " "; width: 100%; bottom: 0; left: 0; border-bottom: 1px solid #AAA; z-index: 1; } ' +
        '.tabrow li:before, .tabrow li:after { border: 1px solid #AAA; position: absolute; bottom: -1px; width: 5px; height: 5px; content: " "; } ' +
        '.tabrow li:before { left: -6px; border-bottom-right-radius: 6px; border-width: 0 1px 1px 0; box-shadow: 2px 2px 0 #D1D1D1; } ' +
        '.tabrow li:after { right: -6px; border-bottom-left-radius: 6px; border-width: 0 0 1px 1px; box-shadow: -2px 2px 0 #D1D1D1; } ' +
        '.tabrow li.selected:before { box-shadow: 2px 2px 0 #FFF; } ' +
        '.tabrow li.selected:after { box-shadow: -2px 2px 0 #FFF; } ';
    document.head.appendChild(tabStyleEle);
    config=makeFloater("Config", ( typeof(localStorage.ConfigFloater) != 'undefined') ? localStorage.ConfigFloater : "0,1"); 
    configForm=config.getElementsByTagName('div')[0];
    var cnb = document.createElement('ul');
    cnb.classList.add("tabrow");
    configForm.appendChild(cnb);
    var configItems=['Display','Misc'];
    for (var tab in configItems ) {
        var d=document.createElement('div');
        var l=document.createElement('li');
        var a=document.createElement('a');
        a.href="#";
        a.addEventListener('click',function(e) { 
            var event = e || window.event; 
           /* event.stopPropagation();*/
            event.preventDefault();
            openconftab(event); 
            if ( debug ) { console.log("done with the clicking");} 
            return false;});
        a.innerHTML=configItems[tab];
        l.appendChild(a);
        cnb.appendChild(l);
        d.id="Config" + configItems[tab];
        d.classList.add("ConfigTab");
        if ( tab > 0 ) {
            d.style.display="none";
        }
        else {
            l.classList.add("selected");
        }
        d.innerHTML=configItems[tab] + " stuff goes here";
        configForm.appendChild(d);
    }
    keyboard=makeFloater("Keyboard",( typeof(localStorage.KeyboardFloater) != 'undefined') ? localStorage.KeyboardFloater : "100,1");
}
setupConfig();
setStylesheet();
if ( commentPage && commentEle ) {
  formatImages(commentEle);
  formatLinks(commentEle);
}
if ( debug ) {
    function debugDrag(event) { console.log("ondrag ",event.target);}
    c = document.getElementById('Config');
    c.addEventListener('ondrag', debugDrag, false);
}

if ( commentPage === true ) {
    tmpStyleEle = document.createElement('style');
    tmpStyleEle.type = 'text/css';
    tmpStyleEle.id = 'commentStylin';
    tmpStyleEle.textContent = 'span.unqClass {  color: #F5C5C5; position: fixed;  }' + '';
    document.head.appendChild(tmpStyleEle);


    var mySpan = document.createElement('span');
    mySpan.id = "unqId";
    mySpan.className = 'unqClass';
    mySpan.innerHTML=commentSystem;
    document.body.insertBefore(mySpan, document.body.childNodes[0]);
}
